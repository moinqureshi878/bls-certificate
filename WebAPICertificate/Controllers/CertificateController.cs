﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using WebAPICertificate.Models;

namespace WebAPICertificate.Controllers
{
    [EnableCors("*", "*", "*")]
    public class CertificateController : ApiController
    {

        CertificateService service = new CertificateService();
        EpCertificateService epService = new EpCertificateService();

        public IHttpActionResult GetCertificate(int id)
        {
            try
            {
                CertificateResponse certificateDto = epService.GetScanned(id);

                return Ok(new
                {
                    result = new
                    {
                        code = 200,
                        message = "Success"
                    },
                    data = new
                    {
                        scanned = certificateDto.Scanned,
                        certificates = certificateDto.Certificate
                    }
                });
            }
            catch (Exception ex)
            {
                return Ok(ex.InnerException.Message.ToString());
            }

        }

        [Route("api/Certificate/GenerateFile")]
        [ActionName("GenerateFile")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage GenerateFile(int id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            dynamic bytes = service.GetCertificate(id).Certificate;
            result.Content = new StreamContent(new MemoryStream(bytes));
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/pdf");
            return result;
        }
        [Route("api/Certificate/GetEpCertificate")]
        [ActionName("GenerateFile")]
        [HttpGet]
        public IHttpActionResult GetEpCertificate(int id)
        {
            try
            {
                CertificateResponse certificateDto = service.GetScanned(id);

                return Ok(new
                {
                    result = new
                    {
                        code = 200,
                        message = "Success"
                    },
                    data = new
                    {
                        scanned = certificateDto.Scanned,
                        certificates = certificateDto.Certificate
                    }
                });
            }
            catch (Exception ex)
            {
                return Ok(ex.InnerException.Message.ToString());
            }

        }

        [Route("api/Certificate/epGenerateFile")]
        [ActionName("GenerateFile")]
        [HttpGet]
        [AllowAnonymous]
        public HttpResponseMessage epGenerateFile(int id)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            dynamic bytes = epService.GetCertificate(id).Certificate;
            result.Content = new StreamContent(new MemoryStream(bytes));
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/pdf");
            return result;
        }
    }
}
