﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Text;
using java.io;
using iTextSharp.tool.xml;
using System.Collections;

namespace WebAPICertificate.Models
{
    public class EpCertificateService
    {
        private int STAFF_LIMIT = Convert.ToInt32(ConfigurationManager.AppSettings["STAFF_LIMIT"].ToString());
        public CertificateResponse GetCertificate(int id)
        {
            try
            {
                List<epHtmlFiles> htmlFiles = new List<epHtmlFiles>();
                epData certificateData = GetCertificateData(id);

                if (certificateData != null)
                {
                    htmlFiles.Add(new epHtmlFiles()
                    {
                        path = HttpContext.Current.Server.MapPath("~/epCertificates/entertainer.html"),
                        name = "main",
                        data = certificateData
                    });
                    htmlFiles.Add(new epHtmlFiles()
                    {
                        path = HttpContext.Current.Server.MapPath("~/epCertificates/two.html"),
                        name = "entertainerDetails",
                        data = certificateData
                    });
                    htmlFiles.Add(new epHtmlFiles()
                    {
                        path = HttpContext.Current.Server.MapPath("~/epCertificates/three.html"),
                        name = "terms",
                        data = certificateData
                    });

                    byte[] certBytes = createPdf(htmlFiles);

                    return new CertificateResponse
                    {
                        Certificate = certBytes,
                        //Scanned = certificateData.Scanned
                    };
                }
                else
                {
                    return new CertificateResponse
                    {
                        Certificate = new byte[0],
                        Scanned = null
                    };
                }
            }
            catch (Exception ex)
            {
                return new CertificateResponse
                {
                    Certificate = new byte[0],
                    Scanned = null
                };
            }
        }
        public CertificateResponse GetScanned(int id)
        {
            try
            {
                List<HtmlFiles> htmlFiles = new List<HtmlFiles>();
                epData certificateData = GetCertificateData(id);

                return new CertificateResponse
                {
                    Certificate = new byte[0]
                    //,
                    //Scanned = certificateData.Scanned
                };
            }
            catch (Exception ex)
            {
                return new CertificateResponse
                {
                    Certificate = new byte[0],
                    Scanned = null
                };
            }
        }
        public static IEnumerable<List<T>> SplitList<T>(List<T> locations, int nSize = 30)
        {
            for (int i = 0; i < locations.Count; i += nSize)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }
        public byte[] createPdf(List<epHtmlFiles> files)
        {
            using (MemoryStream baos = new MemoryStream())
            {
                Document document = new Document();
                PdfCopy copy = new PdfCopy(document, baos);
                document.Open();
                PdfReader reader;
                foreach (epHtmlFiles file in files)
                {
                    byte[] btsPdfIn = parseHtml(file.name, file.path, file.data);
                    if (btsPdfIn.Length > 0)
                    {
                        reader = new PdfReader(btsPdfIn);
                        copy.AddDocument(reader);
                        reader.Close();
                    }
                }
                document.Close();

                return baos.ToArray();
            }
        }
        public byte[] parseHtml(string page, String html, dynamic data)
        {
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document(PageSize.A3, 0f, 0f, 5f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    string fontPath = HttpContext.Current.Server.MapPath("~/Certificates/fonts/SakkalMajalla.ttf");
                    var example_css = @"body{direction:ltr;}";
                    string body = string.Empty;
                    using (StreamReader reader = new StreamReader(html))
                    {
                        body = reader.ReadToEnd();
                        reader.Close();
                    }
                    //SetBody1(body, certificateData);

                    bool pageSkip = false;
                    if (page == "terms")
                    {
                        if (false)
                        {
                            pageSkip = true;
                        }
                        else
                        {
                            GetTermsAndCondition(data, ref body);
                        }
                    }
                    else if (page == "entertainerDetails")
                    {
                        if (Convert.ToBoolean(data.epRequest[0].pt[0].isGroup) == false)
                        {
                            EntertainerDetails(data, ref body);
                        }
                        else
                        {
                            pageSkip = true;
                        }
                    }
                    else if (page == "main")
                    {
                        SetMainBody(ref body, data);
                    }

                    if (pageSkip)
                    {
                        return new byte[0];
                    }

                    //SetDefaultConfig(data, ref body);

                    var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(body));
                    var msCss = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(example_css));
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                    writer.Open();
                    pdfDoc.Open();
                    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, msHtml, msCss, System.Text.Encoding.UTF8, new FontOverrider(fontPath));
                    //writer.Close();
                    pdfDoc.Close();
                    memoryStream.Close();
                    return memoryStream.ToArray();

                }
            }
            catch (Exception ex)
            {
                return new byte[0];
            }
        }
        private string SetDefaultConfig(CertificateDto dto, ref string body)
        {
            if (dto.Config != null)
            {
                body = body.Replace("{header}", dto.Config.Header);
                body = body.Replace("{footer}", dto.Config.Footer);
            }
            return body;
        }
        private string GetInspectorsHtml(CertificateDto dto, ref string body)
        {
            /*
                                         <tr>
                                <td>{8} /<span class=""arabicText"" > {9}</span></td>
                                <td>Profession</td>
                                <td><span class=""arabicText"" > المهنة</span></td>
                            </tr>

        <tr>
                                <td>{10}</td>
                                <td>Health Card No</td>
                                <td><span class=""arabicText"" > رقم البطاقة الصحية</span></td>
                            </tr>
        */
            int maxDataRows = 5;
            string staffHtml = string.Empty;
            string spaces = string.Empty;
            Dictionary<string, string> textAndSpaced = new Dictionary<string, string>();
            int currentRecords = dto.Staff.Count;
            if (dto.Staff.Count > 0)
            {
                foreach (var st in dto.Staff)
                {
                    string data = string.Format(@"<table class=""twocolumns fourcolumns border"">
                            <tr>
                                <th colspan=""2"">
                                </th>
                            </tr>
                            <tr class=""borderBottom"" >
                                <td style=""width: 20%; "" class=""p-0"" >
                                    <img src=""{8}"" alt ="""" class=""setImageSize""/>
                                </td>
                                <td style=""width: 80%;"" class=""p-0"" >
                                    <table class=""innerTable table-bordered"" >
                                        <tr>
                                            <td>
                                                {0} / <span class=""arabicText"" > {1}</span>
                                            </td>
                                            <td>Name</td>
                                            <td><span class=""arabicText"" > الاسم</span></td>
                                        </tr>
                                        <tr>
                                            <td>{2} /<span class=""arabicText"" > {3}</span></td>
                                            <td>Nationality</td>
                                            <td><span class=""arabicText"" > الجنسية</span></td>
                                        </tr>
                                        <tr>
                                            <td>{4}</td>
                                            <td>Passport No</td>
                                            <td><span class=""arabicText"" > رقم الجواز</span></td>
                                        </tr>
                                        <tr>
                                            <td>{5}</td>
                                            <td>U.I.D No</td>
                                            <td><span class=""arabicText"" > الرقم الموحد</span></td>
                                        </tr>
                                        <tr>
                                            <td>{6} /<span class=""arabicText""> {7}</span></td>
                                            <td>Sponsor</td>
                                            <td>الكفيل</td>
                                        </tr>
                                        <tr>
                                            <td>{9}</td>
                                            <td>Health Card No.</td>
                                            <td>رقم البطالة الصحية</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>", st.FullNameEn, st.FullNameAr, st.NationalityEn, st.NationalityAr, st.PassportNo, st.UID, st.SponserName, st.SponserName,
                         st.ProfileImageUrl, st.Religion);
                    staffHtml += data;
                }
            }

            if (currentRecords < STAFF_LIMIT)
            {
                int addSpaces = STAFF_LIMIT - currentRecords;
                if (addSpaces > 0)
                {
                    for (int z = 0; z < addSpaces; z++)
                    {
                        string hiddenTables = string.Format(@"<div class=""hideDiv""></div>");
                        staffHtml += hiddenTables;
                    }
                }
            }
            body = body.Replace("{staff}", staffHtml);
            body = body.Replace("{spaces}", spaces);
            body = body.Replace("{ServiceTypeEn}", dto.Permit.ApplicationTypeEn);
            body = body.Replace("{ServiceTypeAr}", dto.Permit.ApplicationTypeAr);
            body = body.Replace("{TPRefNo}", dto.Permit.TPRefNo);
            body = body.Replace("{PermitGroupEn}", dto.Permit.PermitGroupEn);
            body = body.Replace("{PermitGroupAr}", dto.Permit.PermitGroupAr);
            body = body.Replace("{PermitTypeEn}", dto.Permit.PermitTypeEn);
            body = body.Replace("{PermitTypeAr}", dto.Permit.PermitTypeAr);
            return body;
        }
        private string GetTermsAndCondition(epData dto, ref string body)
        {
            int pageSize = 20;
            //string termsAndConditionMatter = string.Empty;
            //string spaces = string.Empty;
            //Dictionary<string, string> textAndSpaced = new Dictionary<string, string>();
            //if (!string.IsNullOrEmpty(dto.Config.TermsEn) && !string.IsNullOrEmpty(dto.Config.TermsAr))
            //{
            //    string enTerms = dto.Config.TermsEn;
            //    string arTerms = dto.Config.TermsAr;
            //    List<string> lstTermsEn = enTerms.Split('\n').ToList();
            //    lstTermsEn = lstTermsEn.Where(s => !string.IsNullOrEmpty(s)).ToList();
            //    List<string> lstTermsAr = arTerms.Split('\n').ToList();
            //    lstTermsAr = lstTermsAr.Where(s => !string.IsNullOrEmpty(s)).ToList();
            //    if (lstTermsEn.Count == lstTermsAr.Count)
            //    {
            //        for (int z = 0; z < lstTermsEn.Count; z++)
            //        {
            //            termsAndConditionMatter += string.Format(@" <tr>
            //                    <td class=""text-left"">
            //                    {0}
            //                    </td>
            //                    < td class=""text-right"">
            //                        <span class=""arabicText"">{1}</span>
            //                    </td>
            //                </tr>", lstTermsEn[z], lstTermsAr[z]);
            //        }
            //        if (lstTermsEn.Count < pageSize)
            //        {
            //            int addSpaces = pageSize - lstTermsEn.Count;
            //            addSpaces = addSpaces * 2;
            //            if (addSpaces > 0)
            //            {
            //                for (int z = 0; z < addSpaces; z++)
            //                {
            //                    spaces += "<br />";
            //                }
            //            }
            //        }
            //    }
            //}

            //body = body.Replace("{termsAndConditions}", termsAndConditionMatter);
            //body = body.Replace("{spaces}", spaces);
            //body = body.Replace("{ServiceTypeEn}", dto.Permit.ApplicationTypeEn);
            //body = body.Replace("{ServiceTypeAr}", dto.Permit.ApplicationTypeAr);
            //body = body.Replace("{TPRefNo}", dto.Permit.TPRefNo);
            //body = body.Replace("{PermitGroupEn}", dto.Permit.PermitGroupEn);
            //body = body.Replace("{PermitGroupAr}", dto.Permit.PermitGroupAr);
            //body = body.Replace("{PermitTypeEn}", dto.Permit.PermitTypeEn);
            //body = body.Replace("{PermitTypeAr}", dto.Permit.PermitTypeAr);
            return body;
        }

        private string EntertainerDetails(epData dto, ref string body)
        {
            string Tdata = "";
            var entertain = dto.Entertainers;
            for (var a=0;a< entertain.Count; a++)
            {
                var dob = entertain[a].dob.ToString("dd/MM/yyyy").ToString();
                Tdata = Tdata + "< div class='col-md-12'>" +
               "<table class='twocolumns fourcolumns border'>" +
                         "<tr class='borderBottom'>" +
                   "<td style = 'width: 20%;' class='p-0'>" +
                     "<img src = 'https://via.placeholder.com/250x250'/>" +
                   "</td>" +
                   "<td style = 'width: 80%;' class='p-0'>" +
                     "<table class='innerTable table-bordered'>" +
                       "<tr>" +
                         "<td>" +
                            entertain[a].fullNameEN + " / <span class='arabicText'>" + entertain[a].fullNameAR + "</span>" +
                         "</td>" +
                         "<td>Full Name</td>" +
                         "<td><span class='arabicText'>الاسم بالكامل باللغة الانجليزية</span></td>" +
                       "</tr>" +

                       "<tr>" +
                         "<td>"+ entertain[a].PassportNo + "</td>" +
                         "<td>Passport No</td>" +
                         "<td><span class='arabicText'>رقم الجواز</span></td>" +
                       "</tr>" +
                       "<tr>" +
                         "<td>"+ entertain[a].UID + "</td>" +
                         "<td>U.I.D No</td>" +
                         "<td><span class='arabicText'>الرقم الموحد</span></td>" +
                       "</tr>" +
                       "<tr>" +
                         "<td>" + entertain[a].EntertainerPersonTypeID + " /<span class='arabicText'> فندق اس اتش</span></td>" +
                         "<td>Profession</td>" +
                         "<td>المهنة</td>" +
                       "</tr>" +
                       "<tr>" +
                         "<td>" + entertain[a].UID + "</td>" +
                         "<td>Age</td>" +
                         "<td><span class='arabicText'>العمر</span></td>" +
                       "</tr>" +
                       "<tr>" + 
                         "<td>" + dob + "</td>" +
                         "<td>Birth Date</td>" +
                         "<td><span class='arabicText'>تاريخ الميلاد</span></td>" +
                       "</tr>" +

                     "</table>" +
                   "</td>" +
                 "</tr>" +
               "</table>" +
             "</div>";
            }
            body = body.Replace("{EntertainerGrid}", Tdata);

            return body;
        }
        private string SetMainBody(ref string body, epData dto)
        {
            if (dto.epRequest != null)
            {
                var tmp = dto.epRequest[0].RequestID;
                int temp = tmp;
                DateTime startAt = Convert.ToDateTime(dto.epRequest[0].permitStartDate.ToString("dd/MM/yyyy"));

                int remainingPeriod = 0;
                DateTime currentDate = DateTime.UtcNow;
                int twoDateDifference = Convert.ToInt32((currentDate - Convert.ToDateTime(dto.epRequest[0].permitStartDate)).TotalDays);
                remainingPeriod = Convert.ToInt32(dto.epRequest[0].permitPeriodDays) - twoDateDifference;
                //var EntertainerCount = ((IEnumerable)dto.epRequest[0].EntertainerCount).Cast<EntertainerCount>().ToList();

                var ECount = dto.EnterTainerCount;
                int total = 0;

                body = body.Replace("{RequestID}", dto.epRequest[0].RequestID.ToString());
                //body = body.Replace("{ServiceTypeEn}", dto.Permit.ApplicationTypeEn);
                //body = body.Replace("{ServiceTypeAr}", dto.Permit.ApplicationTypeAr);

                body = body.Replace("{PermitGroupEn}", dto.epRequest[0].lp[0].NameEn.ToString());
                body = body.Replace("{PermitGroupAr}", dto.epRequest[0].lp[0].NameAr.ToString());
                body = body.Replace("{PermitTypeEn}", dto.epRequest[0].pt[0].PermitTypeEn.ToString()); 
                body = body.Replace("{PermitTypeAr}", dto.epRequest[0].pt[0].PermitTypeAr.ToString());
                body = body.Replace("{IssueDate}", startAt.ToString("dd/MM/yyyy"));
                body = body.Replace("{StartDate}", Convert.ToDateTime(dto.epRequest[0].permitStartDate.ToString("dd/MM/yyyy")).ToString());
                body = body.Replace("{ExpiryDate}", DateTime.Now.AddDays(remainingPeriod).ToString("dd/MM/yyyy"));
                body = body.Replace("{PermitHolderEmiratesID}", dto.epRequest[0].PermitHolderEmiratesID.ToString());
                body = body.Replace("{PermitHolderFullNameEN}", dto.epRequest[0].PermitHolderFullNameEN.ToString());
                body = body.Replace("{PermitHolderFullNameAR}", dto.epRequest[0].PermitHolderFullNameAR.ToString());
                body = body.Replace("{PermitHolderEmail}", dto.epRequest[0].PermitHolderEmail.ToString());
                body = body.Replace("{PermitHolderMobileNumber}", dto.epRequest[0].PermitHolderMobileNumber.ToString());
                body = body.Replace("{PermitHolderNationalityID}", dto.epRequest[0].PermitHolderNationalityID.ToString());
                body = body.Replace("{WorkLocationEN}", dto.epRequest[0].WorkLocationEN.ToString());
                body = body.Replace("{WorkLocationAR}", dto.epRequest[0].WorkLocationAR.ToString());


                string Table = "<tr><td class='borderBottom text-center'>" +
                  "<p class='strong'>Number of entertainers / <span class='arabicText'> العدد</span> </p></td>" +
                "<td class='borderBottom borderLeft text-center'>" +
                  "<p class='strong'> Entertainer type / <span class='arabicText'> النوع</span></p>" +
                "</td>" +
              "</tr>";
                // + "~myrows~</table>"
                string myrows = "";
                for (var i = 0; i < ECount.Count; i++)
                {
                    total = total + Convert.ToInt32(ECount[i].tp[0].ec[0].EmployeeAddedCount);
                    Table = Table + " <tr class='borderBottom'>"+
                "< td class='text-center'> " + ECount[i].tp[0].ec[0].EmployeeAddedCount + " </td>" +
                "<td class='borderLeft  text-center'>"+
                  "<p class='strong'> "+ ECount[i].EntertainerPersonNameEN + " / <span class='arabicText'> " + ECount[i].EntertainerPersonNameAR + "</span></p>" +
                "</td>"+
              "</tr>";
                }

                //  Table = Table.Replace("~myrows~", myrows);

                body = body.Replace("{EntertainerGrid}", Table);
                body = body.Replace("{Total}", total.ToString());
                //body = body.Replace("{RequestID}", dto.epRequest[0].RequestID);

                //body = body.Replace("{TPRefNo}", dto.Permit.TPRefNo);
                //body = body.Replace("{BusinessNameEn}", dto.Permit.BusinessNameEn);
                //body = body.Replace("{BusinessNameAr}", dto.Permit.BusinessNameAr);
                //body = body.Replace("{FacilityEn}", dto.Permit.FacilityNameEn);
                //body = body.Replace("{FacilityAr}", dto.Permit.FacilityNameAr);

                //body = body.Replace("{LegalTypeEn}", dto.Permit.LegalTypeEn);
                //body = body.Replace("{LegalTypeAr}", dto.Permit.LegalTypeAr);
                //body = body.Replace("{LicenseNo}", dto.Permit.LicenseNo);
                //body = body.Replace("{TPCategoryEn}", dto.Permit.TPCategoryEn);
                //body = body.Replace("{TPCategoryAr}", dto.Permit.TPCategoryAr);
                //body = body.Replace("{AuthEn}", dto.Permit.IssuanceAuthorityEn);
                //body = body.Replace("{AuthAr}", dto.Permit.IssuanceAuthorityAr);
                //body = body.Replace("{ExpiryDateHead}", DateTime.Now.AddDays(remainingPeriod).ToString("dd/MM/yyyy")); //dto.Permit.ExpiryDate.ToShortDateString());
                //body = body.Replace("{IssueDateHead}", startAt.ToString("dd/MM/yyyy")); // dto.Permit.IssueDate.ToShortDateString());
                //body = body.Replace("{PermitHolderURL}", !string.IsNullOrEmpty(dto.Staff.Where(s => s.Type == 727).FirstOrDefault().ProfileImageUrl) ? dto.Staff.Where(s => s.Type == 727).FirstOrDefault().ProfileImageUrl : "https://bls-api-dev.neusol.com/wwwroot/Attachments/2020-12-27%2007-52-40---profile-image.jpg");
                //body = body.Replace("{Passport}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().PassportNo);
                //body = body.Replace("{UID}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().UID);
                //body = body.Replace("{StaffEn}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().FullNameEn);
                //body = body.Replace("{StaffAr}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().FullNameAr);
                //body = body.Replace("{Code}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().PersonalCode);
                //body = body.Replace("{Sponser}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().SponserName);
                //body = body.Replace("{Religion}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().Religion);
                //body = body.Replace("{DOB}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().DateOfBirth.ToShortDateString());
                //body = body.Replace("{Mobile}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().MobileNo);
                //body = body.Replace("{Email}", dto.Staff.Where(s => s.Type == 727).FirstOrDefault().Email);

                //body = body.Replace("{MainAreaEn}", dto.Permit.MainAreaEn);
                //body = body.Replace("{MainAreaAr}", dto.Permit.MainAreaAr);
                //body = body.Replace("{SubAreaEn}", dto.Permit.SubAreaEn);
                //body = body.Replace("{SubAreaAr}", dto.Permit.SubAreaAr);
                //body = body.Replace("{LocationEn}", dto.Permit.WorkLocationEn);
                //body = body.Replace("{LocationAr}", dto.Permit.WorkLocationAr);
                //body = body.Replace("{StreetEn}", dto.Permit.StreetEn);
                //body = body.Replace("{StreetAr}", dto.Permit.StreetAr);
                //body = body.Replace("{Phone}", dto.Permit.PhoneNo);
                //body = body.Replace("{DetailEn}", dto.Permit.PermitDetailsEn);
                //body = body.Replace("{DetailAr}", dto.Permit.PermitDetailsAr);

                //body = body.Replace("{PO}", dto.Permit.POBox);
                //body = body.Replace("{Web}", dto.Permit.Website);
                //body = body.Replace("{Fax}", dto.Permit.FaxNo);
                //body = body.Replace("{Web}", dto.Permit.PhoneNo);
                //body = body.Replace("{BuildingNameEn}", dto.Permit.BuildingNameEn);
                //body = body.Replace("{BuildingNameAr}", dto.Permit.BuildingNameAr);


                //body = body.Replace("{TPGroupEn}", dto.Permit.TPHeadPermitGroupEn);
                //body = body.Replace("{TPGroupAr}", dto.Permit.TPHeadPermitGroupAr);
                //body = body.Replace("{ExpiryDate}", dto.Permit.ExpiryDate.ToString());
                //body = body.Replace("{IssueDate}", dto.Permit.IssueDate.ToString());
                //body = body.Replace("{PrintDate}", DateTime.Now.ToShortDateString());
                //body = body.Replace("{ReceiptDate}", DateTime.Now.ToShortDateString());
                //body = body.Replace("{ReceiptNo}", "");
            }
            return body;
        }
        public epData GetCertificateData(int id)
        {
            epData epData = new epData();
            try
            {
                List<SqlParameter> spParameter = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@RequestID", SqlDbType = SqlDbType.Int, Value = id}
                };

                string outputrRsult = "@Result";
                dynamic list = "";

                list = GetSPData("proc_EP_Certificate_GetDataByRequestId", spParameter, outputrRsult);
               // string data;

                if (list != null && list != "")
                {

                    epData = JsonConvert.DeserializeObject<epData>(list);
                }

                return epData;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public dynamic GetSPData(string query2, List<SqlParameter> sqlParameters, string parameter)
        {
            try
            {
                using (var db = new DB_A470DF_TDA_DevEntities())
                {
                    SqlConnection connection = (SqlConnection)db.Database.Connection;
                    string sp = query2;
                    string jsonOutputParam = parameter;
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sp, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        //cmd.Parameters.AddWithValue("@tab", 1);
                        if (sqlParameters != null)
                            cmd.Parameters.AddRange(sqlParameters.ToArray());
                        // Create output parameter. "-1" is used for nvarchar(max)
                        cmd.Parameters.Add(jsonOutputParam, SqlDbType.NVarChar, -1).Direction = ParameterDirection.Output;

                        // Execute the command
                        cmd.ExecuteNonQuery();

                        // Get the values
                        dynamic json = cmd.Parameters[jsonOutputParam].Value.ToString();
                        return json;
                        // Console.WriteLine(json);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DateTime GetStartAt(int id)
        {
            using (var db = new DB_A470DF_TDA_DevEntities())
            {
                return db.BN_InstanceSteps.Where(x => x.TPRequestID == id && x.StepCode == "Approved").Select(x => x.StartAt).FirstOrDefault();
            }
        }
    }
    public class epHtmlFiles
    {
        public string name { get; set; }
        public string path { get; set; }
        public dynamic data { get; set; }
    }

    
}
