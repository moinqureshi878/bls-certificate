﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPICertificate.Models
{
    public class CertificateDto
    {
        public PermitDto Permit { get; set; }
        public List<StaffDto> Staff { get; set; }
        public ConfigDto Config { get; set; }
        public ICollection<ScannedDto> Scanned { get; set; }
    }
    public class PermitDto
    {
        public int ID { get; set; }
        public string TPRefNo { get; set; }
        public int BLID { get; set; }
        public string BLRefNo { get; set; }
        public int TPHeadID { get; set; }
        public string TPHeadRefNo { get; set; }
        public string TPHeadPermitGroupEn { get; set; }
        public string TPHeadPermitGroupAr { get; set; }
        public string TPHeadPermitTypeEn { get; set; }
        public string TPHeadPermitTypeAr { get; set; }
        public int PermitGroup { get; set; }
        public string PermitGroupEn { get; set; }
        public string PermitGroupAr { get; set; }
        public int PermitType { get; set; }
        public string PermitTypeEn { get; set; }
        public string PermitTypeAr { get; set; }
        public int TPCategoryID { get; set; }
        public string TPCategoryEn { get; set; }
        public string TPCategoryAr { get; set; }
        public string LicenseNo { get; set; }
        public int IssuanceAuthority { get; set; }
        public string IssuanceAuthorityEn { get; set; }
        public string IssuanceAuthorityAr { get; set; }
        public string BusinessNameEn { get; set; }
        public string BusinessNameAr { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int LegalType { get; set; }
        public string LegalTypeEn { get; set; }
        public string LegalTypeAr { get; set; }
        public int Status { get; set; }
        public string TPHeadPermitLicenseNo { get; set; }
        public string TPHeadPermitBusinessNameEn { get; set; }
        public string TPHeadPermitBusinessNameAr { get; set; }
        public string TPHeadPermitDetailsEn { get; set; }
        public string TPHeadPermitDetailsAr { get; set; }
        public string TPHeadPermitFacilityNameEn { get; set; }
        public string TPHeadPermitFacilityNameAr { get; set; }
        public DateTime TPHeadPermitStartDate { get; set; }
        public int TPHeadPermitPeriodDays { get; set; }
        public string PermitDetailsEn { get; set; }
        public string PermitDetailsAr { get; set; }
        public string FacilityNameEn { get; set; }
        public string FacilityNameAr { get; set; }
        public DateTime PermitStartDate { get; set; }
        public int PermitPeriodDays { get; set; }
        public int ApplicationTypeID { get; set; }
        public string ApplicationTypeEn { get; set; }
        public string ApplicationTypeAr { get; set; }
        public int MainAreaID { get; set; }
        public string MainAreaEn { get; set; }
        public string MainAreaAr { get; set; }
        public int SubAreaID { get; set; }
        public string SubAreaEn { get; set; }
        public string SubAreaAr { get; set; }
        public string StreetEn { get; set; }
        public string StreetAr { get; set; }
        public string WorkLocationEn { get; set; }
        public string WorkLocationAr { get; set; }
        public string BuildingNameEn { get; set; }
        public string BuildingNameAr { get; set; }
        public string PhoneNo { get; set; }
        public string FaxNo { get; set; }
        public string Website { get; set; }
        public string POBox { get; set; }
    }

    public class CertificateResponse
    {
        public byte[] Certificate { get; set; }
        public ICollection<ScannedDto> Scanned { get; set; }
    }

    public class StaffDto
    {
        public int ID { get; set; }
        public int TPID { get; set; }
        public string TPRefNo { get; set; }
        public int Type { get; set; }
        public string TypeEn { get; set; }
        public string TypeAr { get; set; }
        public string ProfileImageUrl { get; set; }
        public string FullNameEn { get; set; }
        public string FullNameAr { get; set; }
        public string UID { get; set; }
        public string PassportNo { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string Nationality { get; set; }
        public string NationalityEn { get; set; }
        public string NationalityAr { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Religion { get; set; }
        public string SponserName { get; set; }
        public string PersonalCode { get; set; }
    }

    public class ConfigDto
    {
        public string Header { get; set; }
        public string Footer { get; set; }
        public string TermsEn { get; set; }
        public string TermsAr { get; set; }
        public string BlockStamp { get; set; }
        public string SuspendStamp { get; set; }
        public string MortgageStamp { get; set; }

    }

    public static class ConnectionString
    {
        public static string Value { get; set; }
    }

    public class ScannedDto
    {
        public string AttachmentURL { get; set; }
    }


    /// <summary>
    /// ////////////////////////////////////////ep//////////////////////
    /// </summary>
    /// 

   
    public class epData
    {
        public dynamic epRequest { get; set; }
        public List<EnterTainerCount> EnterTainerCount { get; set; }
        public dynamic Entertainers { get; set; }
    }
    public class EnterTainerCount
    {
        public string EntertainerPersonNameEN { get; set; }
        public string EntertainerPersonNameAR { get; set; }
        public dynamic tp { get; set; }
    }

}