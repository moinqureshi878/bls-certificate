﻿using iTextSharp.text;
using iTextSharp.text.pdf;


namespace WebAPICertificate
{
    internal class FontOverrider : FontFactoryImp
    {
        private readonly BaseFont baseFont;

        /// <summary>
        /// Create a new font factory that always uses the provided font.
        /// </summary>
        /// <param name="fullPathToFontFileToUse">The full path to the font file to use.</param>
        /// <param name="encoding">The type of encoding to use. Default BaseFont.IDENTITY_H. See <see cref="http://api.itextpdf.com/itext/com/itextpdf/text/pdf/BaseFont.html#createFont(java.lang.String, java.lang.String, boolean)"/> for details.</param>
        /// <param name="embedded">Whether or not to embed the entire font. Default True. See <see cref="http://api.itextpdf.com/itext/com/itextpdf/text/pdf/BaseFont.html#createFont(java.lang.String, java.lang.String, boolean)"/> for details.</param>
        public FontOverrider(string fullPathToFontFileToUse, string encoding = BaseFont.IDENTITY_H, bool embedded = BaseFont.EMBEDDED)
        {
            //If you are using this class then this font is required and a missing font should be a fatal error
            if (!System.IO.File.Exists(fullPathToFontFileToUse))
            {
                throw new System.IO.FileNotFoundException("Could not find the supplied font file", fullPathToFontFileToUse);
            }

            //Create our embedded base font
            baseFont = BaseFont.CreateFont(fullPathToFontFileToUse, encoding, embedded);

        }

        public override iTextSharp.text.Font GetFont(string fontname, string encoding, bool embedded, float size, int style, BaseColor color, bool cached)
        {
            return new iTextSharp.text.Font(baseFont, size, style, color);
        }
    }
}